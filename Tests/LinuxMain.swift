import XCTest

import designSystemTests

var tests = [XCTestCaseEntry]()
tests += designSystemTests.allTests()
XCTMain(tests)
