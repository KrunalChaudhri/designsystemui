// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "designSystem",
    platforms: [
        .macOS(.v10_12),
        .iOS(.v13),
        .tvOS(.v13),
        .watchOS(.v3)
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(name: "designSystem",targets: ["designSystem"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        .package(url: "https://github.com/tadija/AEXML.git", from: "4.6.0")
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "designSystem",
            dependencies: ["AEXML"],
            resources: [.process("colors.json"),.process("Styler.json"),.process("KML_Sample.kml"),.process("Resources"),.process("TipPopView.xib")]),
        .testTarget(
            name: "designSystemTests",
            dependencies: ["designSystem","AEXML"]),
    ]
)
