//
//  klCustomTxtField.swift
//  KlaraSPM
//
//  Created by Apple on 24/12/20.
//

import Foundation
import UIKit

public class klCustomTxtField: UITextField {
    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        customInit()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }

    @IBInspectable
    public var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }

    @IBInspectable
    public var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }

    @IBInspectable
    public var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }

    @IBInspectable
    public var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }

    @IBInspectable
    public var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }

    @IBInspectable
    public var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }

    @IBInspectable
    public var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }

    public func customInit() {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 30))
        leftView = paddingView
        leftViewMode = .always
//        placeholder = "Password...."
//        isSecureTextEntry = true
//        autocorrectionType = .no
//        autocapitalizationType = .none
//        attributedPlaceholder = NSAttributedString(string: "Password....", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
//        tintColor = .white
//        borderStyle = .none
//        layer.borderColor = UIColor.white.cgColor
//        layer.borderWidth = 0.7
//        textColor = .white
    }
}
