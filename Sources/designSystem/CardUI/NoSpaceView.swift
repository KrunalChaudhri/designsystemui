//
//  NoSpaceView.swift
//  KLARA UI
//
//  Created by Pranav Panchal on 28/09/20.
//

import UIKit

#if SWIFT_PACKAGE
let nospaceViewbundle = Bundle.module
#else
let nospaceViewbundle = Bundle(for: NoSpaceView.self)
#endif

@IBDesignable public class NoSpaceView: UIView {
    
    var view: UIView!
    @IBOutlet weak var cardview: UIView!
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
    
    func loadViewFromNib() {
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: nospaceViewbundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        view.frame = bounds
        view.autoresizingMask = [
            UIView.AutoresizingMask.flexibleWidth,
            UIView.AutoresizingMask.flexibleHeight
        ]
        addSubview(view)
        self.view = view
    }
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    public override func draw(_ rect: CGRect) {
        // Drawing code
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2).cgColor
    }
}
