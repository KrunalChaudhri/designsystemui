//
//  BottomBarView.swift
//  RegioApp
//
//  Created by Pranav Panchal on 31/10/20.
//

import UIKit

protocol BottomBarButtonDelegate {
    func homeButtonClick()
    func searchButtonClick()
    func scanButtonClick()
    func heartButtonClick()
    func lastButtonClick()
    func logoButtonClick()
    func avtarButtonClick()
}

#if SWIFT_PACKAGE
let bottombarbundle = Bundle.module
#else
let bottombarbundle = Bundle(for: BottomBarView.self)
#endif

@IBDesignable public class BottomBarView: UIView {
    
    @IBOutlet weak var contentView: UIView!
    var delegate: BottomBarButtonDelegate?

    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var tabView: UIView!
    @IBOutlet weak var tabBarView: UIView!
    @IBOutlet weak var appTitle: UILabel!
    
    @IBOutlet weak var homeBtn: UIButton!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var scanBtn: UIButton!
    @IBOutlet weak var heartBtn: UIButton!
    @IBOutlet weak var lastBtn: UIButton!
    @IBOutlet weak var logoBtn: UIButton!
    @IBOutlet weak var avtarBtn: UIButton!

    
    /*// Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }*/
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    func setup() {
        let nib = UINib(nibName: "BottomBarView", bundle: bottombarbundle)
        nib.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        addSubview(contentView)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
                 
        let layer = self.applyGradientLinear(colors:[colorsData(named: "Gradient_1_Primary"),colorsData(named: "Dark_Primary")])
        self.bottomView.layer.insertSublayer(layer, at: 0)
        
        tabView.normalViewShadow()
        appTitle.font = UIFont().robotoStyle(style:.bold, size: .h5)
        scanBtn.normalViewShadow()
    }
    
    //Button Action Methods
    @IBAction func homeBtnClick()   { delegate?.homeButtonClick()}
    @IBAction func searchBtnClick() { delegate?.searchButtonClick()}
    @IBAction func scanBtnClick()   { delegate?.scanButtonClick()}
    @IBAction func heartBtnClick()  { delegate?.heartButtonClick()}
    @IBAction func lastBtnClick()   { delegate?.lastButtonClick()}
    @IBAction func logoBtnClick()   { delegate?.logoButtonClick()}
    @IBAction func avtarBtnClick()  { delegate?.avtarButtonClick()}
    
}
