//
//  TipPopView.swift
//  RegioApp
//
//  Created by Piyush Agrawal on 18/10/20.
//

import UIKit

#if SWIFT_PACKAGE
let frameworkBundle = Bundle.module
#else
let frameworkBundle = Bundle(for: TipPopView.self)
#endif

public class TipPopView: UIView {
    var view: UIView!
    var close_press: (() -> Void)?

    @IBOutlet var lbl1: UILabel!
    @IBOutlet var lbl2: UILabel!

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init(coder aCoder: NSCoder) {
        super.init(coder: aCoder)!
        setup()
    }

    func setup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        lbl1.font = UIFont().robotoStyle(style:.bold, size: .h5)
        lbl2.font = UIFont().robotoStyle(style:.regular, size: .h5)
        
        lbl1.font = UIFont().robotoStyle(style:.bold, size: .h5)
        
        addSubview(view)
    }

    func loadViewFromNib() -> UIView {
        return UINib(nibName: "TipPopView", bundle: frameworkBundle).instantiate(withOwner: self, options: nil).first as! UIView
    }

    @IBAction private func close_tapped() {
        if let buttonAction = close_press {
            buttonAction()
        }
    }

    func updateConstraintslayout() {
        layoutIfNeeded()
    }
}
