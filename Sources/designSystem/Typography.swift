//
//  Typography.swift
//  KLARA UI
//
//  Created by Apple on 20/09/20.
//

import Foundation
import UIKit


public class RobotoFonts {
    public enum  size : Int {
        case h2 = 32
        case h5 = 16
        case h6 = 14
        case small = 12
    }
    public enum style : String {
        case regular = "Roboto-Regular"
        case bold = "Roboto-Bold"
    }
}

extension UIFont {
    public func robotoStyle(style : RobotoFonts.style , size : RobotoFonts.size) -> UIFont {
        return UIFont(name: style.rawValue, size: CGFloat(size.rawValue))!
    }
}

//extension UIFont {
//
//    public enum styles: String {
//        case extraboldItalic = "-ExtraboldItalic"
//        case semiboldItalic = "-SemiboldItalic"
//        case semibold = "-Semibold"
//        case regular = "Roboto-Regular"
//        case lightItalic = "Light-Italic"
//        case light = "-Light"
//        case italic = "-Italic"
//        case extraBold = "-Extrabold"
//        case boldItalic = "-BoldItalic"
//        case bold = "Roboto-Bold"
//    }
//
//    public enum size : Int {
//        case h2 = 32
//        case h5 = 16
//        case h6 = 14
//        case small = 12
//    }
//
//    public func robotoStyle(_ type: styles = .regular, size : size) -> UIFont {
//        return UIFont(name: "OpenSans\(type.rawValue)", size: CGFloat(size.rawValue))!
//    }
//
//    var isBold: Bool {
//        return fontDescriptor.symbolicTraits.contains(.traitBold)
//    }
//
//    var isItalic: Bool {
//        return fontDescriptor.symbolicTraits.contains(.traitItalic)
//    }
//
//}
