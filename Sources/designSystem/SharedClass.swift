//
//  File.swift
//  
//
//  Created by Apple on 31/12/20.
//

import Foundation
import UIKit

public class SharedClass: NSObject {//This is shared class
public static let sharedInstance = SharedClass()
    public func printMessage(){
        print("hello guys")
    }
    //Show alert
    public func alert(view: UIViewController, title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: { action in
        })
        alert.addAction(defaultAction)
        DispatchQueue.main.async(execute: {
            view.present(alert, animated: true)
        })
    }

    public override init() {
    }
}
