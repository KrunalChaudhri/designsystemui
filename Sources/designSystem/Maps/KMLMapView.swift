//
//  KMLMapView.swift
//  SwissMapDemo
//
//  Created by Pranav Panchal on 19/11/20.
//

import MapKit
import UIKit

open class KMLMapView: MKMapView, MKMapViewDelegate {
    public var mapLayers = MapLayers()
    public var showGreenLayer: Bool = true

    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override public func draw(_ rect: CGRect) {
        // Drawing code
        delegate = self
        mapLayers.InitialMapLoading(mapView: self)
    }

    public func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay.isKind(of: MKCircle.self) {
            if overlay.coordinate.latitude.isEqual(to: mapView.centerCoordinate.latitude) {
                let view = MKCircleRenderer(overlay: overlay)
                view.fillColor = UIColor.white
                return view
            } else {
                let view = MKCircleRenderer(overlay: overlay)
                view.fillColor = UIColor(red: 171.0 / 255.0, green: 229.0 / 255.0, blue: 46.0 / 255.0, alpha: 0.5)
                view.strokeColor = UIColor(red: 171.0 / 255.0, green: 229.0 / 255.0, blue: 46.0 / 255.0, alpha: 1.0)
                view.lineWidth = 2
                return view
            }
        }

        if let overlayPolyline = overlay as? KMLOverlayPolyline {
            // return MKPolylineRenderer
            return overlayPolyline.renderer()
        }

        if let overlayPolygon = overlay as? KMLOverlayPolygon {
            // return MKPolygonRenderer
            if showGreenLayer {
                overlayPolygon.style?.polyStyle?.color = mapLayers.greenColor
            } else {
                overlayPolygon.style?.polyStyle?.color = mapLayers.greyColor
            }
            return overlayPolygon.renderer()
        }

        if let tileOverlay = overlay as? MKTileOverlay {
            return MKTileOverlayRenderer(tileOverlay: tileOverlay)
        } else {
            return MKOverlayRenderer(overlay: overlay)
        }
    }

    public func mapViewDidChangeVisibleRegion(_ mapView: MKMapView) {
        print("visible region called")

        let zoomWidth = mapView.visibleMapRect.size.width
        let zoomFactor = Int(log2(zoomWidth)) - 9

        print("zoom factor: \(zoomFactor)")

        if zoomFactor <= 11 {
            if mapView.isScrollEnabled == true {
                return
            }
            mapLayers.removeAllMapLayers(mapView: mapView)
            mapView.isScrollEnabled = true
            mapLayers.addStyleToMapView(mapView: mapView)
        }

        if zoomFactor == 12 {
            if mapView.isScrollEnabled == false {
                return
            }

            mapLayers.removeAllMapLayers(mapView: mapView)
            mapLayers.addOverlays(mapView: mapView)
            mapView.addOverlays(mapLayers.kml_overlays)
            mapView.isScrollEnabled = false
        }
    }
}
