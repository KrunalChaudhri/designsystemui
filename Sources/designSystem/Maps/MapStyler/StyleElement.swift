//
//  StyleElement.swift
//  Pods
//
//  Created by Fernando on 1/2/17.
//
//

import Foundation

public protocol StyleElement {
    var rawValue: String {get}
    var convertedValue: String {get}
}
