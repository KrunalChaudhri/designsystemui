//
//  GradientButton.swift
//  KLARA UI
//
//  Created by Apple on 27/09/20.
//

import UIKit
import Foundation

@IBDesignable public class GradientButton: UIButton {
    
    public enum style {
        case normal,disabled
    }
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    //let subLayer = CALayer()
    lazy var gradientLayer: CAGradientLayer! = {
        self.applyGradientLinear(colors: [colorsData(named: "Gradient_1_Primary"),colorsData(named: "Dark_Primary")])
    }()
    
    @IBInspectable var isDisabled: Bool = false {
        didSet {
            self.setup(styled: isDisabled ? .disabled : .normal)
        }
    }
    
    
    override init(frame:CGRect){
        super.init(frame: frame)
        self.setup()
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    
    public func setup(styled:style = .normal) {
        
        clipsToBounds = true
        layer.cornerRadius = self.frame.height/2
        layer.masksToBounds = false
        self.titleLabel?.font = UIFont().robotoStyle(style:.regular, size: .h5)
        
        if styled == .disabled{
            //self.isUserInteractionEnabled = false
            self.backgroundColor = colorsData(named: "GrayPale_Neutral")
            self.setTitleColor(colorsData(named: "Gray_Neutral"), for: .normal)
        }
        else{
            self.isUserInteractionEnabled = true
            self.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
            //setShadowLayer()
            setGradientBackground()
            
        }
        self.addTarget(self, action: #selector(press_effect), for: .touchUpInside)
        
    }
    
    public func setGradientBackground() {
        
        //gradientLayer = self.applyGradientLinear(colors:[UIColor(named: "Gradient_1_Primary")!,UIColor(named: "Dark_Primary")!])
        
        
        gradientLayer.frame = bounds
        gradientLayer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: self.frame.height/2).cgPath
        gradientLayer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.4).cgColor
        gradientLayer.shadowOpacity = 1
        gradientLayer.shadowRadius = 5
        gradientLayer.shadowOffset = CGSize(width: 0, height: 0)
        
        layer.insertSublayer(gradientLayer!, at: 0)
    }
    
    /*
    private func setShadowLayer(){
        
        subLayer.frame = bounds
        subLayer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: self.frame.height/2).cgPath
        subLayer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.4).cgColor
        subLayer.shadowOpacity = 1
        subLayer.shadowRadius = 5
        subLayer.shadowOffset = CGSize(width: 0, height: 0)
        
        //layer.insertSublayer(subLayer, below: gradientLayer!)
    }*/
    
    public func changeStyle(toStyle:style = .normal){
        //subLayer.removeFromSuperlayer()
        gradientLayer.removeFromSuperlayer()
        self.setup(styled: toStyle)
    }
    
    @objc public func press_effect(){
        //animateShadow()
        //animateGradient()
 
        let animation = CABasicAnimation(keyPath: "shadowOpacity")
        animation.fromValue = layer.shadowOpacity
        animation.toValue = 0.0
        animation.duration = 0.5
        
        let gradientChangeAnimation = CABasicAnimation(keyPath: "colors")
        gradientChangeAnimation.fromValue = gradientLayer!.colors
        gradientChangeAnimation.toValue = [colorsData(named: "Dark_Primary"),colorsData(named: "Dark_Primary")]

        let group = CAAnimationGroup()
        group.animations = [animation,gradientChangeAnimation]
        group.duration = 0.5
        group.isRemovedOnCompletion = true
        group.fillMode = CAMediaTimingFillMode.forwards
        //group.delegate = self
        self.gradientLayer!.add(group,forKey: "groupAnim")
        
    }

}

