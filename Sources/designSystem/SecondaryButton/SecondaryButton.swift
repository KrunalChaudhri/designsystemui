
//
//  SecondaryButton.swift
//  KLARA UI
//
//  Created by Apple on 27/09/20.
//

import UIKit

@IBDesignable public class SecondaryButton: UIButton, CAAnimationDelegate {
    
    public enum style {
        case normal,disabled
    }
    
    let subLayer = CAShapeLayer()
    
    @IBInspectable var isDisabled: Bool = false {
        didSet {
            self.setup(styled: isDisabled ? .disabled : .normal)
        }
    }
    
    override init(frame:CGRect){
        super.init(frame: frame)
        self.setup()
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    public func setup(styled : style = .normal) {
        
        clipsToBounds = false
        layer.cornerRadius = self.frame.height/2
        layer.masksToBounds = false
        self.titleLabel?.font = UIFont().robotoStyle(style:.regular, size: .h5)
        
        if styled == .disabled{
            self.isUserInteractionEnabled = false
            self.backgroundColor = colorsData(named: "GrayPale_Neutral")
            self.setTitleColor(colorsData(named: "Gray_Neutral"), for: .normal)
        }
        else{
            self.isUserInteractionEnabled = true
            self.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            self.setTitleColor(colorsData(named: "Primary_Primary")!, for: .normal)
            setShadowLayer()
            
        }
        self.addTarget(self, action: #selector(press_effect), for: .touchUpInside)
        
    }
    
    public func setShadowLayer(){
        subLayer.frame = bounds
        subLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: self.frame.height/2).cgPath
        subLayer.fillColor = backgroundColor!.cgColor
        
        subLayer.shadowColor =  #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.4).cgColor
        // subLayer.shadowPath = subLayer.path
        subLayer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        subLayer.shadowOpacity = 1.0
        subLayer.shadowRadius = 5
        //subLayer.shouldRasterize = true
        //subLayer.rasterizationScale = UIScreen.main.scale
        
        layer.insertSublayer(subLayer, at: 0)
    }
    
    public func changeStyle(toStyle:style = .normal){
        subLayer.removeFromSuperlayer()
        self.setup(styled: toStyle)
    }
    
    @objc public func press_effect(){
//        animateShadow()
//        animateBackgroundColor()
        
        
        self.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        
        let animation = CABasicAnimation(keyPath: "shadowOpacity")
        animation.fromValue = layer.shadowOpacity
        animation.toValue = 0.0
        //animation.duration = 0.5
        //self.subLayer.add(animation, forKey: animation.keyPath)
        //self.subLayer.shadowOpacity = 1.0
        
        let animcolor = CABasicAnimation(keyPath: "fillColor")
        animcolor.fromValue = backgroundColor!.cgColor
        animcolor.toValue = colorsData(named: "Dark_Primary")
        //animcolor.duration = 0.5
        //animcolor.repeatCount = 0
        //animcolor.autoreverses = true
        //subLayer.add(animcolor, forKey: "fillColor")
        
        let group = CAAnimationGroup()
        group.animations = [animation,animcolor]
        group.duration = 0.5
        group.isRemovedOnCompletion = true
        group.delegate = self
        self.subLayer.add(group,forKey: "groupAnim")
    }
    
    public func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        self.setTitleColor(colorsData(named: "Primary_Primary"), for: .normal)
    }
    
}
