//
//  File.swift
//
//
//  Created by Apple on 01/01/21.
//

import Foundation
import UIKit

public let fontBundle = Bundle.module
////public let bundleURL = Bundle.main.url(forResource: "Colors", withExtension: "xcassets")
////public let bundle = Bundle(url: bundleURL!)
//public let blueColorData = UIColor(named: "Gradient_1_Primary", in: fontBundle, compatibleWith: nil)
//public let darkData = UIColor(named: "Dark_Primary", in: fontBundle, compatibleWith: nil)

public func colorsData(named name: String) -> UIColor! {
  UIColor(named: name, in: Bundle.module, compatibleWith: nil)
}

public func imagesData(named name: String) -> UIImage! {
  UIImage(named: name, in: Bundle.module, compatibleWith: nil)
}

public func registerFonts() {
    _ = UIFont.registerFont(bundle: .module, fontName: "Roboto-Black", fontExtension: "ttf")
    _ = UIFont.registerFont(bundle: .module, fontName: "Roboto-BlackItalic", fontExtension: "ttf")
    _ = UIFont.registerFont(bundle: .module, fontName: "Roboto-Bold", fontExtension: "ttf")
    _ = UIFont.registerFont(bundle: .module, fontName: "Roboto-BoldItalic", fontExtension: "ttf")
    _ = UIFont.registerFont(bundle: .module, fontName: "Roboto-Light", fontExtension: "ttf")
    _ = UIFont.registerFont(bundle: .module, fontName: "Roboto-Regular", fontExtension: "ttf")
}

var myData: Data!

func checkFile() {
    if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last {
        let fileURL = documentsDirectory.appendingPathComponent("YourFile.extension")
        do {
            let fileExists = try fileURL.checkResourceIsReachable()
            if fileExists {
                print("File exists")
            } else {
                print("File does not exist, create it")
                writeFile(fileURL: fileURL)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
}

func writeFile(fileURL: URL) {
    do {
        try myData.write(to: fileURL)
    } catch {
        print(error.localizedDescription)
    }
}

extension UIFont {
    static func registerFont(bundle: Bundle, fontName: String, fontExtension: String) -> Bool {
        guard let fontURL = bundle.url(forResource: fontName, withExtension: fontExtension) else {
            fatalError("Couldn't find font \(fontName)")
        }

        guard let fontDataProvider = CGDataProvider(url: fontURL as CFURL) else {
            fatalError("Couldn't load data from the font \(fontName)")
        }

        guard let font = CGFont(fontDataProvider) else {
            fatalError("Couldn't create font from data")
        }

        var error: Unmanaged<CFError>?
        let success = CTFontManagerRegisterGraphicsFont(font, &error)
        guard success else {
            print("Error registering font: maybe it was already registered.")
            return false
        }

        return true
    }
}
